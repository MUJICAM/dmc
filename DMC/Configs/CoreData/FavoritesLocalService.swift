//
//  FavoritesLocalService.swift
//  DMC
//
//  Created by MUJICAM on 9/1/22.
//

//import Foundation
import CoreData
import UIKit

class FavoritesLocalService {
    
    var context: NSManagedObjectContext
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    // MARK: - Create new Feed Favorite
    func create(feed: FeedModel) -> Bool {
        
        let newItem = NSEntityDescription.insertNewObject(forEntityName: "LocalFavoriteFeed", into: context) as? LocalFavoriteFeed
        let newItemContact = NSEntityDescription.insertNewObject(forEntityName: "LocalContact", into: context) as? LocalContact
        newItem?.id = "\(feed.id)"
        newItem?.title = feed.title
        newItem?.image = feed.image
        newItem?.descriptionFeed = feed.description
        newItem?.published = feed.published
        newItem?.author_id = feed.author_id
        newItemContact?.id = "\(feed.contact.id)"
        newItemContact?.lastName = feed.contact.lastName
        newItemContact?.firstName = feed.contact.firstName
        newItemContact?.gender = feed.contact.gender
        newItem?.contact = newItemContact
        return saveChanges()
    }
    
    // MARK: - Gets by id
    func getById(id: NSManagedObjectID) -> LocalFavoriteFeed? {
        return context.object(with: id) as? LocalFavoriteFeed
    }
    
    // MARK: - Gets all.
    func getAll() -> [LocalFavoriteFeed]{
        return get(withPredicate: NSPredicate(value:true))
    }
    
    // MARK: - Gets all that fulfill the specified predicate.
    ///     Predicates examples:
    ///     - NSPredicate(format: "name == %@", "Juan Carlos")
    ///     - NSPredicate(format: "name contains %@", "Juan")
    func get(withPredicate queryPredicate: NSPredicate) -> [LocalFavoriteFeed]{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "LocalFavoriteFeed")
        
        fetchRequest.predicate = queryPredicate
        
        do {
            let response = try context.fetch(fetchRequest)
            return response as! [LocalFavoriteFeed]
            
        } catch let error as NSError {
            // failure
            print(error)
            return [LocalFavoriteFeed]()
        }
    }
    
    // MARK: - Deletes a Feed
    func delete(id: NSManagedObjectID) -> Bool {
        if let bankToDelete = getById(id: id){
            context.delete(bankToDelete)
            return saveChanges()
        }else {
            return false
        }
    }
    
    // MARK: - Saves all changes
    func saveChanges() -> Bool{
        do{
            try context.save()
            return true
        } catch let error as NSError {
            // failure
            print(error)
            return false
        }
    }
}

extension NSManagedObject {
  func toJSON() -> String? {
    let keys = Array(self.entity.attributesByName.keys)
    let dict = self.dictionaryWithValues(forKeys: keys)
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        let reqJSONStr = String(data: jsonData, encoding: .utf8)
        return reqJSONStr
    }
    catch{}
    return nil
  }
}

//
//  API.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import Foundation

enum APIConfigs: String {
    case fileName = "URL"
    case extensionFile = "plist"
    case API_URL = "API_URL"
    case BASE_URL = "BASE_URL"
}

class API {
    
    static let startNode = "data"
    static let error = "error"
    static let message = "message"
    static let status = "status"
    static let success = "exito"
    static let failure = "fallo"
    static let valid = "valid"
    
    func getAPIURL() -> String{
        let fileName = APIConfigs.fileName.rawValue
        let extensionFile = APIConfigs.extensionFile.rawValue
        let url = APIConfigs.API_URL.rawValue
        if let path = Bundle.main.path(forResource: fileName, ofType: extensionFile){
            if let URLSDict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String>{
                if let baseURL =  URLSDict[url]{
                    return baseURL
                }
            }
        }
        return ""
    }
    
    static func parseErrorMessage(dict: [String: Any]) -> String {
        let field = (dict["fieldsErrors"] as? [[String: Any]] ?? [[:]]).first?["field"] as? String ?? ""
        let constraint = ((dict["fieldsErrors"] as? [[String: Any]] ?? [[:]]).first?["constraints"] as? [String])?.first ?? ""
        
        let errorMessage = "\(field): \(constraint)"
        
        if !errorMessage.isEmpty {
            return errorMessage
        }
        
        return Localizables.Alert.standardError
    }
    
    static func tokenHeaders(withToken token: String) -> [String:String] {
        var headers: [String:String] = ["Content-Type":"application/json"]
        headers["apikey"] = token
        return headers
    }
    
    static func tokenHeadersMultipart(withToken token: String) -> [String:String] {
        var headers: [String:String] = ["Content-Type":"multipart/form-data"]
        headers["Authorization"] = token
        return headers
    }
    
    struct Auth {
        enum AuthPaths {
            case signIN
        }
        
        static func getPath(for type: AuthPaths) -> String {
            var path = ""
            switch type {
            case .signIN:
                path = "login"
            }
            
            return API().getAPIURL() + path
        }
    }
    
    struct Feed {
        enum FeedPaths {
            case getFeed
        }
        
        static func getPath(for type: FeedPaths) -> String {
            var path = ""
            switch type {
            case .getFeed:
                path = "feed"
            }
            
            return API().getAPIURL() + path
        }
    }
    
    struct Contact {
        enum ContactPath {
            case getContact
        }
        
        static func getPath(for type: ContactPath) -> String {
            var path = ""
            switch type {
            case .getContact:
                path = "contacts"
            }
            
            return API().getAPIURL() + path
        }
    }
    
    struct Favorites {
        enum FavotiesPath {
            case setFavorites
        }
        
        static func getPath(for type: FavotiesPath) -> String {
            var path = ""
            switch type {
            case .setFavorites:
                path = "favorite"
            }
            
            return API().getAPIURL() + path
        }
    }
}

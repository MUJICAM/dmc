//
//  AppManager.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import Foundation
import UIKit
import CoreData

class AppManager: NSObject, UITabBarControllerDelegate {
    
    enum AuthMode: Int {
        case loggedOut = 0
        case loggedIn = 1
    }
    
    var rootViewController: UIViewController?
    static let shared = AppManager()
    
    fileprivate var _tabBarController: CustomTabBarController?
    var tabBarController: CustomTabBarController {
        get {
            if (_tabBarController == nil) {
                _tabBarController = CustomTabBarController()
                // Here we add the views
                let controller1 = ViewFactory.viewControllerForAppView(.home)
                let controller2 = ViewFactory.viewControllerForAppView(.favorites)
                _tabBarController?.viewControllers = [controller1, controller2]
                //here tabBar settings
                _tabBarController?.tabBar.isTranslucent = false
                _tabBarController?.tabBar.barStyle = .black
                _tabBarController?.tabBar.barTintColor = UIColor(rgb: 0x0D0F11)
                _tabBarController?.tabBar.tintColor = .white
                _tabBarController?.tabBar.unselectedItemTintColor = .darkGray
                _tabBarController?.view.backgroundColor = UIColor(rgb: 0x0D0F11)
                _tabBarController?.tabBar.itemPositioning = .centered
            }

            _tabBarController?.selectedIndex = 0
            return _tabBarController!
        }
    }
    
    //This prevents others from using the default '()' initializer for this class.
    fileprivate override init() {
        super.init()
        setRootForCurrentAuthMode()
    }
    
    func userDidLogin() {
        resetRootController()
    }

    func userDidLogout() {
        // Clear current session's data
        Storage.removeStorage()
        Storage.removeUserStorage()
        deleteAllRecords()
        UserDefaults.standard.synchronize()
        resetRootController()
    }
    
    func deleteAllRecords(_ entityName : String = "LocalFavoriteFeed") {
        //delete all data
        let context = appDelegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    func resetRootController() {
        rootViewController = nil
        _tabBarController = nil
        setRootForCurrentAuthMode()
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            rootViewController?.view.alpha = 0
            appDelegate.window?.rootViewController = rootViewController
            appDelegate.window?.makeKeyAndVisible()
            UIView.animate(withDuration: 0.2, animations: {
                self.rootViewController?.view.alpha = 1
            })
        }
    }
    
    func setRootForCurrentAuthMode() {
        switch authMode() {
        case .loggedOut:
            // Set to the desired baseViewController for logged out status
            let vc = ViewFactory.viewControllerForAppView(.rootInital)
            rootViewController = vc
            break
        case .loggedIn:
            // Set to the desired baseViewController for logged in status
            rootViewController = tabBarController
            break
        }
    }
    
    func authMode() -> AuthMode {
        // Set login status of user if it's saved on the device
        if let _ = Storage.sessionData {
            return .loggedIn
        }else{
            return .loggedOut
        }
    }
    
}

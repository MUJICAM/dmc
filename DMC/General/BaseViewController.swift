//
//  BaseViewController.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    var hasTopNotch: Bool {
        if #available(iOS 13.0,  *) {
            return UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.safeAreaInsets.top ?? 0 > 20
        }
        
        return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
    }

    var hasBottomNotch: Bool {
        if #available(iOS 13.0,  *) {
            let bottom = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.safeAreaInsets.bottom ?? 0
            return bottom > 0
        } else {
            return false
        }
    }
}

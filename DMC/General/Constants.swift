//
//  Constants.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import UIKit
import CoreData

// MARK: - CoreData
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let context = appDelegate.persistentContainer.viewContext
/// Favorites Feed
var favoritesFeedData = FavoritesLocalService.init(context: context)

// MARK: - kViewControllers
struct kViewControllers {
    
    //XIBs file names
    struct General {
        static let launchVC = "LaunchVC"
        static let rootInitial = "LoginVC"
        static let home = "HomeVC"
        static let detailsFeedVC = "DetailsFeedVC"
        static let favoritesVC = "FavoritesVC"
    }
    
}


// MARK: - Localizables
struct Localizables {
    
    struct Alert {
        static let unavailableTagging = NSLocalizedString("alerts.unavailable_tagging", comment: "")
        static let unavailableFeature = NSLocalizedString("alerts.unavailable_feature", comment: "")
        static let unavailableContent = NSLocalizedString("alerts.unavailable_content", comment: "")
        static let cancel = NSLocalizedString("alerts.cancel", comment: "")
        static let accept = NSLocalizedString("alerts.accept", comment: "")
        static let error = NSLocalizedString("alerts.error", comment: "")
        static let alert = NSLocalizedString("alerts.alert", comment: "")
        static let standardError = NSLocalizedString("alerts.standard_error", comment: "")
        static let userUpdated = NSLocalizedString("alerts.user_updated", comment: "")
        static let ok = NSLocalizedString("alerts.ok", comment: "")
        static let saveSuccess = NSLocalizedString("saveSuccess", comment: "")
        static let existsFavorites = NSLocalizedString("existsFavorites", comment: "")
    }
    
    static func cellLocalizables(cellType: CellsEnum) -> String {
        switch cellType {
        case .textField(let cellsStyles):
            switch cellsStyles {
            case .username: return NSLocalizedString("text_field_cell.username", comment: "")
            case .password: return NSLocalizedString("text_field_cell.password", comment: "")
            default: return NSLocalizedString("", comment: "invalid localizable")
            }
        case .button(let cellsStyles):
            switch cellsStyles {
            case .login: return NSLocalizedString("button_cell.login", comment: "")
            default: return NSLocalizedString("", comment: "invalid localizable")
            }
        default: return ""
        }
    }
    
    struct ValidationMessages {
        static let emptyTextMessage = NSLocalizedString("validation_messages.empty_text_message", comment: "")
        static let allFieldsEmpty = NSLocalizedString("validation_messages.all_fields_empty", comment: "")
        static let emptyValue = NSLocalizedString("validation_messages.empty_value", comment: "")
        static let password = NSLocalizedString("validation_messages.password.too_short", comment: "")
        static let invalidPhone = NSLocalizedString("validation_messages.invalid_phoneNumber", comment: "")
        
        struct ContentCreation {
            static let url =  NSLocalizedString("validation_messages.content_creation.url", comment: "")
            static let caption =  NSLocalizedString("validation_messages.content_creation.caption", comment: "")
            static let expirationDate =  NSLocalizedString("validation_messages.content_creation.expiration_date", comment: "")
        }
        
        static func fixedValidationMessages(for style: CellsStyles) -> String {
            switch style {
            case .password:
                return NSLocalizedString("validation_messages.password.too_short", comment: "")
            default: break
            }
            
            return ""
        }
    }
    
    struct modalsMessage {
        static let logout = NSLocalizedString("logout", comment: "")
        static let logout_Message = NSLocalizedString("logout_Message", comment: "")
        static let yes = NSLocalizedString("yes", comment: "")
        
        static let menu = NSLocalizedString("menu", comment: "")
        static let addFavorites = NSLocalizedString("addFavorites", comment: "")
        static let removeFavorite = NSLocalizedString("removeFavorite", comment: "")
        static let addFavoritesMessage = NSLocalizedString("addFavoritesMessage", comment: "")
    }
    
    struct detailsFeed{
        static let description = NSLocalizedString("description", comment: "")
    }
}

// MARK: - Fonts
enum FontTypes {
    case bold
    case light
    case medium
    case regular
    case semibold
    case thin
}

enum FontFamily {
    case Sans
    case Mono
}

func setFont(of type: FontTypes, family: FontFamily = .Sans, and size: CGFloat) -> UIFont {
    var fontName = ""
    
    switch family {
    case .Mono:
        fontName = "IBMPlexMono"
        break
    case .Sans:
        fontName = "IBMPlexSans"
        break
    }
    
    switch type {
    case .bold:
        fontName += "-Bold"
    case .light:
        fontName += "-Light"
    case .medium:
        fontName += "-Medium"
    case .regular:
        fontName += ""
    case .semibold:
        fontName += "-SemiBold"
    case .thin:
        fontName += "-Thin"
    }
    
    let font = UIFont(name: fontName, size: size)!
    return font
}

// MARK: - Constants
struct Constants {
    static let appPrefix = "DCM"
    static let companyName = "MujicaMStudio"
    static let serverTimeFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
}


// MARK: - Colors
struct Colors {
    static let statusBarColor = UIColor(rgb: 0x191C1F)
    static let primaryColor = UIColor(rgb: 0x1B88E6)
    static let highlightedDark = UIColor(rgb: 0x212223).withAlphaComponent(0.5)
    static let lightBlue = UIColor(rgb: 0x0088CD)
    static let lightRed = UIColor(rgb: 0xed6060)
    static let gray = UIColor(rgb: 0x888888)
    static let blackPrimary = UIColor(rgb: 855826)
    static let greyText = UIColor(rgb: 0x72787D)
    
    struct Gradient {
        static var primary = GradientColor(colorA: UIColor(rgb: 0x1B88E6), colorB: UIColor(rgb: 0x00F5FF))
        static var secundary = GradientColor(colorA: UIColor(rgb: 0xF54B64), colorB: UIColor(rgb: 0x941BE0))
        static var shearchGradient = GradientColor(colorA: UIColor(rgb: 0xF54B64), colorB: UIColor(rgb: 0x941BE0))
    }
    
    static let inputFocusedColorsArray: [UIColor] = [
        UIColor(rgb: 0xb10f8e),
        UIColor(rgb: 0x984b8c),
        //UIColor(rgb: 0x7b8e97),
        UIColor(rgb: 0x00bba9)
    ]
    
}

struct GradientColor {
    var colorA: UIColor = .white
    var colorB: UIColor = .black
    
    init(colorA: UIColor, colorB: UIColor) {
        self.colorA = colorA
        self.colorB = colorB
    }
}

enum AIEdge:Int {
    case
    Top,
    Left,
    Bottom,
    Right,
    Top_Left,
    Top_Right,
    Bottom_Left,
    Bottom_Right,
    All,
    None
}


//MARK: - CellsEnum
enum CellsEnum {
    
    // General Table/Collection View Cells used throughout the app
//    case label              (CellsStyles)
    case textField          (CellsStyles)
    case button             (CellsStyles)
    case blankSpace         (CellsStyles)
    case image              (CellsStyles)
    case feed               (FeedModel)
    case feedFavorite       (LocalFavoriteFeed)
}

enum CellsStyles: CaseIterable {
    case none
    
    //LabelCell
    case usernameTitle
    case passwordTitle
    
    //TextFieldCell
    case username
    case password
    
    //Blankspace
    case extraLarge
    case large
    case small
    case medium
    case tiny
    case smallBlack
    case ultraTiny
    
    //ButtonCell
    case login
}

struct Cells {
    static func identifiersMethod(type: CellsEnum) -> String {
        switch type {
//        case .label: return "LabelCell"
        case .textField: return "TextFieldCell"
        case .button: return "ButtonCell"
        case .blankSpace: return "UITableViewCell"
        case .image: return "ImageCell"
        case .feed, .feedFavorite: return "FeedCell"
        }
    }
    
    //MARK: - CellsHeight
    static func preferredHeight(type: CellsEnum) -> CGFloat {
        switch type {
        case .textField: return 100
        case .button: return 60
        case .blankSpace(let cellsStyles):
            switch cellsStyles {
            case .extraLarge: return 240.0
            case .large: return 120.0
            case .medium: return 80.0
            case .small: return 40.0
            case .tiny: return 20.0
            case .smallBlack: return 40.0
            case .ultraTiny : return 2.0
            default: return 0
            }
        case .image: return 200
        case .feed, .feedFavorite: return 284
        }
    }
    
    static func styleToKey(style: CellsStyles) -> String {
        switch style {
        case .username: return "username"
        case .password: return "password"
        default: return ""
        }
    }
}

// MARK: - Images
struct Images {
    
    enum icons: String {
        case buttonContinue = "button.continue.active"
        case logout = "logout"
        case menu = "menu"
        case user = "user"
        case backWhite = "backWhite"
    }
    
    enum images: String {
        case login = "LOGIN"
    }
    
    enum tabBarIcons: String {
        case home = "home"
        case favorites = "favorito"
    }
    
    static func icon(type: icons) -> UIImage? {
        return UIImage(named: "\(type.rawValue)")
    }
    
    static func image(type: images) -> UIImage? {
        return UIImage(named: "\(type.rawValue)")
    }
    
    static func tabBarIcon(type: tabBarIcons) -> UIImage? {
        return UIImage(named: "\(type.rawValue)")
    }
}

func setHTMLDescription(with codeHTML: String) -> NSAttributedString {
    let htmlString = """
        <html>
            <head>
                <style>
                    body {
                        background-color : rgb(250, 250, 250);
                        color            : #FFFFFF;
                        font-family      : 'IBMPlexSans-Bold';
                        text-decoration  : none;
                        font-size        : 14;
                    }
                </style>
            </head>
            <body>
                \(codeHTML)
            </body>
        </html>
        """
    let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)

    let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]

    let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)

    return attributedString
}

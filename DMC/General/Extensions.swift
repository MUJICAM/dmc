//
//  Extensions.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import Foundation
import UIKit

// MARK: - UIView
extension UIView {
    
    // MARK: - Inspectables
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor {
        get {
            return self.borderColor
        }
        set {
            layer.borderColor = newValue.cgColor
        }
    }
    
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var circular: Bool {
        get {
            return self.circular
        }
        set {
            if newValue {
                self.layer.cornerRadius = self.frame.size.width/2
                self.clipsToBounds = true
            }
        }
    }
    
    @IBInspectable var shadow: Bool{
        get{ return false }
        set{
            
            if newValue {
                self.layer.masksToBounds = false
                self.layer.shadowColor = UIColor.black.cgColor
                self.layer.shadowOpacity = 0.12
                self.layer.shadowOffset = CGSize(width: 0, height: 0)
                self.layer.shadowRadius = 5
            }else{
                self.layer.masksToBounds = true
            }
        }
    }
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat, shadow: Bool = false) {
        DispatchQueue.main.async {
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
            
            if shadow {
                let shadowView: UIView = UIView()
                shadowView.backgroundColor = self.backgroundColor
                shadowView.layer.cornerRadius = radius
                shadowView.layer.shadowColor = UIColor.black.cgColor
                shadowView.layer.shadowOffset = CGSize(width: 0, height: 3.0)
                shadowView.layer.shadowOpacity = 0.5
                shadowView.layer.shadowRadius = 5
                self.superview?.insertSubview(shadowView, belowSubview: self)
                shadowView.frame = self.frame
            }
        }
    }
    
    func fadeIn(at duration: TimeInterval, completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration, animations: { [weak self] in
            self?.alpha = 1
        }) { (_) in
            completion?()
        }
    }
    
    func fadeOut(at duration: TimeInterval, completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration, animations: { [weak self] in
            self?.alpha = 0
        }) { (_) in
            completion?()
        }
    }
    
    func horizontalGradient(colors: [UIColor]) {
        
        var cgColorsArray: [CGColor] = []
        
        for color in colors {
            cgColorsArray.append(color.cgColor)
        }
        
        // Initialize gradient layer.
        let gradientLayer: CAGradientLayer = CAGradientLayer()

        gradientLayer.frame = self.bounds
        gradientLayer.colors = cgColorsArray
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func horizontalGradient(gradientColor: GradientColor) {
        backgroundColor = .clear
        let gradient = CAGradientLayer()
        gradient.colors = [gradientColor.colorA.cgColor, gradientColor.colorB.cgColor]
        gradient.locations = [0.0, 1.0]
        gradient.frame = bounds
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.cornerRadius = layer.cornerRadius
        layer.insertSublayer(gradient, at: 0)
    }
}

// MARK: - UIColor
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    
    convenience init(rgb: Int, a: CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            a: a
        )
    }
}

// MARK: - Range Expression
extension RangeExpression where Bound == String.Index  {
    func nsRange<S: StringProtocol>(in string: S) -> NSRange { .init(self, in: string) }
}

// MARK: - String
extension String {
    
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
        case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        case hasUppercase = ".*[A-Z]+.*"
        case hasNumber = ".*[0-9]+.*"
        case hasLowercase = ".*[a-z]+.*"
        case hasSpecialChar = ".*[$&+,:;=?@#|’<>.^*()%!-]+.*"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    
    func camelCaseToWords() -> String {
        
        return unicodeScalars.reduce("") {
            
            if CharacterSet.uppercaseLetters.contains($1) {
                
                return ($0 + " " + String($1)).replacingOccurrences(of: "Blue", with: "")
            }
            else {
                
                return ($0 + String($1)).replacingOccurrences(of: "Blue", with: "")
            }
        }
    }
    
}


// MARK: - UITableView
extension UITableView {
    
    //This method allow to register cells ONLY of the
    //.xib and the cell class has the same name
    
    func registerCell(named: String) {
        let identifier = named
        let nib = UINib(nibName: identifier, bundle: nil)
        self.register(nib, forCellReuseIdentifier: identifier)
    }
}


//UINavigationController
extension UINavigationController {
    func getPreviousViewController() -> UIViewController? {
        let count = viewControllers.count
        guard count > 1 else { return nil }
        return viewControllers[count - 1]
    }
    
    func pushViewController(viewController: UIViewController, animated: Bool, completion: @escaping () -> Void) {
        pushViewController(viewController, animated: animated)

        if animated, let coordinator = transitionCoordinator {
            coordinator.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }

    func popViewController(animated: Bool, completion: @escaping () -> Void) {
        popViewController(animated: animated)

        if animated, let coordinator = transitionCoordinator {
            coordinator.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }
}

// MARK: - UIViewController
extension UIViewController {
    
    enum ButtonDirections {
        case leftButton
        case rightButton
    }
    
    func setNav(icon: UIImage?, to button: ButtonDirections, target: Any?, action: Selector?, color: UIColor = .white, font: UIFont? = nil, and title: String = "", speacing addSpacing: Double = 0) {
        
        let barButton = UIBarButtonItem(image: icon, style: .plain, target: target, action: action)
        
        switch button {
        case .leftButton:
            navigationItem.leftBarButtonItem = barButton
            navigationItem.leftBarButtonItem?.tintColor = color
            navigationItem.leftBarButtonItem?.title = title
            
            
            
            if let font = font {
                navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: font, .foregroundColor: color], for: .normal)
                navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: font, .foregroundColor: color], for: .highlighted)
                navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: font, .foregroundColor: color], for: .selected)
            }
            
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.kern: addSpacing], for: .normal)
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.kern: addSpacing], for: .highlighted)
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.kern: addSpacing], for: .selected)
            break
        default:
            navigationItem.rightBarButtonItem = barButton
            navigationItem.rightBarButtonItem?.tintColor = color
            navigationItem.rightBarButtonItem?.title = title
            if let font = font {
                navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: font, .foregroundColor: color], for: .normal)
                navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: font, .foregroundColor: color], for: .highlighted)
                navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: font, .foregroundColor: color], for: .selected)
            }
            
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.kern: addSpacing], for: .normal)
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.kern: addSpacing], for: .highlighted)
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.kern: addSpacing], for: .selected)
            break
        }
        

    }
    
    func setCustomNav(customView: UIView, to button: ButtonDirections) {
        let barButton = UIBarButtonItem(customView: customView)
        
        switch button {
        case .leftButton:
            navigationItem.leftBarButtonItem = barButton
            break
        default:
            navigationItem.rightBarButtonItem = barButton
        }
    }
    
    func showOptionsAlert(title: String?, blueButtonTitle: String, redButtonTitle: String, message: String, positiveBlock: @escaping() -> Void, cancelBlock: @escaping() -> Void) {
        let refreshAlert = UIAlertController(
            title: title ?? "",
            message: message,
            preferredStyle: .alert
        )
        
        refreshAlert.addAction(UIAlertAction(title: blueButtonTitle, style: .default, handler: { _ in
            positiveBlock()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: redButtonTitle, style: .destructive, handler: { _ in
            cancelBlock()
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
}

// MARK: - UITextView
extension UITextView {
    func setScrollToTop() {
        DispatchQueue.main.async {
            self.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: false)
        }
    }
}

// MARK: - TimeInterval
extension TimeInterval {
    func chronometerFormat() -> String {
        let minutes = Int(self) / 60 % 60
        let seconds = Int(self) % 60
        
        return String(format:"%02i:%02i", minutes, seconds)
    }
}

extension UILabel {
    
    func addCharacterSpacing(kernValue: Double = 1.15) {
        if let labelText = text, labelText.count > 0 {
            let attributedString = NSMutableAttributedString(string: labelText)
            attributedString.addAttribute(NSAttributedString.Key.kern, value: kernValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
}

//MARK: - Date
extension Date {
    func timeAgoDisplay() -> String {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .abbreviated
        return formatter.localizedString(for: self, relativeTo: Date())
    }
}

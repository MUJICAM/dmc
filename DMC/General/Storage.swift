//
//  Storage.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import Foundation
import ObjectMapper


class Storage {
    
    fileprivate static let lastUserLoggedInKey = "LAST_USER"
    fileprivate static let contactKey = "contacts"
    
    class var contact: [Contacts]? {
        get {
            if let userString = UserDefaults.standard.string(forKey: contactKey) {
                return Mapper<Contacts>().mapArray(JSONString: userString)
            }
            return  nil
        }
        
        set {
            UserDefaults.standard.set(newValue?.toJSONString(), forKey: contactKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var sessionData: SignUser? {
        get {
            if let userString = UserDefaults.standard.string(forKey: lastUserLoggedInKey) {
                return Mapper<SignUser>().map(JSONString: userString)
            }
            return  nil
        }
        set {
            UserDefaults.standard.set(newValue?.toJSONString(), forKey: lastUserLoggedInKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    class func removeStorage() {
        UserDefaults.standard.removeObject(forKey: contactKey)
        UserDefaults.standard.removeObject(forKey: lastUserLoggedInKey)
    }
    
    class func removeUserStorage() {
        UserDefaults.standard.removeObject(forKey: lastUserLoggedInKey)
    }
}

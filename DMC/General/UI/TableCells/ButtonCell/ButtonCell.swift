//
//  ButtonCell.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import UIKit

protocol ButtonCellDelegate {
    func mainButtonPressed(_ sender: UIButton, cell: ButtonCell, style: CellsStyles)
}

class ButtonCell: UITableViewCell {
    
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!

    var style: CellsStyles = .none
    var delegate: ButtonCellDelegate?
    var auxiliarIsHidden : Bool = true
    
    private var _isEnabled: Bool = false
    var isEnabled: Bool {
        get {
            return _isEnabled
        }
        
        set {
            _isEnabled = newValue
            UIView.animate(withDuration: 0.5, animations: {
                self.mainButton.alpha = newValue ? 1 : 0.5
                self.mainButton.isEnabled = newValue
            })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        
    }
    
    func setupCell(with style: CellsStyles) {
        self.style = style
        
        mainButton.setTitle(Localizables.cellLocalizables(cellType: .button(style)), for: .normal)
        mainButton.titleLabel?.font = setFont(of: .semibold, and: 15)
        mainButton.cornerRadius = 10
        mainButton.layer.borderColor = UIColor.gray.cgColor
        mainButton.setTitleColor(.white, for: .normal)
        mainButton.addTarget(self, action: #selector(mainButtonPressed(_:)), for: .touchUpInside)
        
        switch style {
        case .login:
            widthConstraint.constant = UIScreen.main.bounds.width - (25 * 2)
            mainButton.setBackgroundImage(Images.icon(type: .buttonContinue), for: .normal)
            mainButton.setTitle("", for: .normal)
        default:
            break
        }
    }
    
    @objc func mainButtonPressed(_ sender: UIButton) {
        delegate?.mainButtonPressed(sender, cell: self, style: self.style)
    }
}

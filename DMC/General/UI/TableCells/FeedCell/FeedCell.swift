//
//  FeedCell.swift
//  DMC
//
//  Created by MUJICAM on 8/1/22.
//

import UIKit
import SDWebImage

protocol FeedDelegate: AnyObject {
    func favoritesButtonPressed(_ sender: UIButton, cell: FeedCell, feed: FeedModel)
    func LocalFavoritesButtonPressed(_ sender: UIButton, cell: FeedCell, feed: LocalFavoriteFeed)
}

extension FeedDelegate {
    func favoritesButtonPressed(_ sender: UIButton, cell: FeedCell, feed: FeedModel){}
    func LocalFavoritesButtonPressed(_ sender: UIButton, cell: FeedCell, feed: LocalFavoriteFeed){}
}

class FeedCell: UITableViewCell {
    
    @IBOutlet weak var imageFavorites: UIImageView!
    @IBOutlet weak var labelAuthor: UILabel!
    @IBOutlet weak var buttonFavorites: UIButton!
    @IBOutlet weak var imageFeed: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageUser: UIImageView!
    
    var delegate: FeedDelegate?
    var feed: FeedModel?
    var localFeed: LocalFavoriteFeed?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup() {
        imageFavorites.image = Images.icon(type: .menu)
        imageFeed.image = nil
        labelDate.text = "loading..."
        labelTitle.text = "loading..."
        labelAuthor.text = "loading..."
        buttonFavorites.titleLabel?.text = ""
        buttonFavorites.setTitle("", for: .normal)
        
        labelAuthor.font = setFont(of: .bold, and: 12)
        labelAuthor.textColor = .white
        labelTitle.font = setFont(of: .bold, and: 14)
        labelTitle.textColor = .white
        labelDate.font = setFont(of: .semibold, family: .Mono,and: 10)
        labelDate.textColor = .yellow.withAlphaComponent(0.35)
    }

    func setupCell(feed: FeedModel) {
        self.feed = feed
        
        imageFeed.sd_setImage(
            with: feed.imageURL,
            placeholderImage: Images.image(type: .login),
            options: [],
            completed: nil
        )
        
        imageUser.image = Images.icon(type: .user)
        imageFavorites.image = Images.icon(type: .menu)
        labelAuthor.text = "\(feed.contact.firstName) \(feed.contact.lastName)"
        labelDate.text = feed.formattedDate.uppercased()
        labelTitle.text = feed.title
        
        buttonFavorites.addTarget(self, action: #selector(favoritesButtonPressed(_:)), for: .touchUpInside)
    }
    
    func setupCell(feed: LocalFavoriteFeed) {
        self.localFeed = feed
        
        imageFeed.sd_setImage(
            with: URL(string: feed.image ?? ""),
            placeholderImage: Images.image(type: .login),
            options: [],
            completed: nil
        )
        
        imageUser.image = Images.icon(type: .user)
        imageFavorites.image = Images.icon(type: .menu)
        labelAuthor.text = "\(feed.contact?.firstName ?? "") \(feed.contact?.lastName ?? "")"
        let serverFormat = DateFormatter()
        serverFormat.dateFormat = Constants.serverTimeFormat
        serverFormat.timeZone = TimeZone(identifier: "GMT")
        
        if let date = serverFormat.date(from: feed.published ?? "") {
            labelDate.text = date.timeAgoDisplay()            
        }
        labelTitle.text = feed.title
        
        buttonFavorites.addTarget(self, action: #selector(LocalFavoritesButtonPressed(_:)), for: .touchUpInside)
    }
    
    @objc func favoritesButtonPressed(_ sender: UIButton) {
        guard let feed = feed else { return }
        delegate?.favoritesButtonPressed(sender, cell: self, feed: feed)
    }
    
    @objc func LocalFavoritesButtonPressed(_ sender: UIButton) {
        guard let feed = localFeed else { return }
        delegate?.LocalFavoritesButtonPressed(sender, cell: self, feed: feed)
    }
}

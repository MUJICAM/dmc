//
//  ImageCell.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import UIKit

class ImageCell: UITableViewCell {

    @IBOutlet weak var mainImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        mainImage.image = nil
    }

    func setupCell(with style: CellsStyles) {
        switch style{
        case .login:
            mainImage.image = Images.image(type: .login)
        default:
            break
        }
    }
}

//
//  TextFieldCell.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import UIKit

protocol TextFieldCellDelegate: AnyObject {
    func textFieldDidChange(_ textField: UITextField, cell: TextFieldCell, style: CellsStyles)
}

class TextFieldCell: UITableViewCell {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var indicatorView: UIView!
    
    weak var delegate: TextFieldCellDelegate?
    var inactiveAlpha: CGFloat = 0.3
    var style: CellsStyles = .none
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    private func setupUI() {
        self.indicatorView.backgroundColor = UIColor.white.withAlphaComponent(0.35)
        titleLabel.textColor = .white
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.autocorrectionType = .no
        
        titleLabel.font = setFont(of: .regular, family: .Mono, and: 12)
        textField.font = setFont(of: .regular, and: 16)
        textField.textColor = .white
        textField.keyboardAppearance = .dark
        textField.textAlignment = .left
    }
    
    func setupCell (with style: CellsStyles, showValidationMessage: Bool = false) {
        self.style = style
        
        titleLabel.text = Localizables.cellLocalizables(cellType: .textField(style)).uppercased()
        titleLabel.addCharacterSpacing(kernValue: 1.5)
        
        textField.isSecureTextEntry = false
        textField.delegate = self
        
        if ((textField.text ?? "").isEmpty) {
            indicatorView.layer.sublayers = []
            indicatorView.backgroundColor = UIColor.white.withAlphaComponent(0.35)
        }
        
        switch style {
        case .password:
            textField.isSecureTextEntry = true
            textField.textColor = .white
            titleLabel.textColor = .white
        case .username:
            textField.textColor = .white
            titleLabel.textColor = .white
        default:
            break
        }
    }
    
    @objc func textFieldDidChange(_ textfield: UITextField) {
        let text = textField.text ?? ""
        
        if text.isEmpty {
            indicatorView.layer.sublayers = []
            indicatorView.backgroundColor = UIColor.white.withAlphaComponent(0.35)
        } else {
            if style == .username {
                self.indicatorView.horizontalGradient(colors: Colors.inputFocusedColorsArray)
            }else{
                self.indicatorView.horizontalGradient(gradientColor: Colors.Gradient.secundary)
            }
        }
        
        delegate?.textFieldDidChange(textField, cell: self, style: self.style)
    }
}

extension TextFieldCell: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.25, animations: {
            self.indicatorView.alpha = 1
            if self.style == .username {
                self.indicatorView.horizontalGradient(colors: Colors.inputFocusedColorsArray)
            }else{
                self.indicatorView.horizontalGradient(gradientColor: Colors.Gradient.secundary)
            }
            self.titleLabel.textColor = .white
        })
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.25, animations: {
            self.indicatorView.layer.sublayers = []
            self.indicatorView.backgroundColor = UIColor.white.withAlphaComponent(0.35)
            self.indicatorView.alpha = 1
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch style {
        case .username, .password:
            if string == " " {
                return false
            }
        default:
            break
        }
        
        return true
    }
}

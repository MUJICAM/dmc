//
//  FavoritesPresenter.swift
//  DMC
//
//  Created by MUJICAM on 10/1/22.
//

import Foundation

protocol FavoritesView: AnyObject {
    func presenterDidRemoveFavorite(status: Bool)
}

class FavoritesPresenter {
    weak var view: FavoritesView?
    
    init(view: FavoritesView){
        self.view = view
    }
    
    func removeFavorite(feed: LocalFavoriteFeed) {
        var query = favoritesFeedData.getAll()
        query = favoritesFeedData.get(withPredicate: NSPredicate(format: "id == %@", "\(feed.id ?? "")"))
        guard let feedToDeleteFromFavorites = query.first else {
            self.view?.presenterDidRemoveFavorite(status: false)
            return
        }
        self.view?.presenterDidRemoveFavorite(status: favoritesFeedData.delete(id: feedToDeleteFromFavorites.objectID))
    }
}

//
//  FavoritesVC.swift
//  DMC
//
//  Created by MUJICAM on 8/1/22.
//

import UIKit
import ObjectMapper

class FavoritesVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var cells: [CellsEnum] = []
    var presenter: FavoritesPresenter?
    var feedList: [LocalFavoriteFeed] = []
    var feedSelected: LocalFavoriteFeed = LocalFavoriteFeed()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = FavoritesPresenter(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupModel()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        for cell in cells {
            tableView.registerCell(named: Cells.identifiersMethod(type: cell))
        }
    }
    
    private func setupModel(){
        self.cells.removeAll()
        self.feedList.removeAll()
        self.feedList = favoritesFeedData.getAll()
        for feed in feedList {
            cells.append(.feedFavorite(feed))
        }
        
        setupTableView()
        tableView.reloadData()
    }
}


extension FavoritesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let type = cells[index]
        let id = Cells.identifiersMethod(type: type)
        switch type {
        case .feedFavorite(let feed):
            let cell = tableView.dequeueReusableCell(withIdentifier: id) as! FeedCell
            cell.setupCell(feed: feed)
            cell.selectionStyle = .none
            cell.delegate = self
            return cell
        default:
            let cell = UITableViewCell()
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ViewFactory.viewControllerForAppView(.details) as! DetailsFeedVC
        vc.localFeed = feedList[indexPath.row]
        vc.delegate = self
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension FavoritesVC: FavoritesView {
    func presenterDidRemoveFavorite(status: Bool) {        
        setupModel()
        tableView.reloadData()
        Toast (
            text: Localizables.Alert.saveSuccess,
            container: navigationController,
            backgroundColor: Colors.lightBlue
        )
    }
}

extension FavoritesVC: FeedDelegate {
    func LocalFavoritesButtonPressed(_ sender: UIButton, cell: FeedCell, feed: LocalFavoriteFeed) {
        let alert = UIAlertController(
            title: Localizables.modalsMessage.menu,
            message: Localizables.modalsMessage.addFavoritesMessage,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(
            title: Localizables.modalsMessage.removeFavorite,
            style: .destructive,
            handler: { [weak self] (UIAlertAction) in
                self?.presenter?.removeFavorite(feed: feed)
            }))
        
        alert.overrideUserInterfaceStyle = .dark
        alert.addAction(UIAlertAction(
            title: Localizables.Alert.cancel,
            style: .cancel ,
            handler:{ (UIAlertAction) in }))
        
        self.present(alert, animated: true, completion: { })
    }
}

extension FavoritesVC: detailsDelegate {
    func reloadTable() {
        setupModel()
    }
}

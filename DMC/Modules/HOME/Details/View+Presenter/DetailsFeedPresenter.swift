//
//  DetailsFeedPresenter.swift
//  DMC
//
//  Created by MUJICAM on 9/1/22.
//

import Foundation

protocol DetailsFeedView: AnyObject {
    func presenterDidFail(errorMessage: String)
    func presenterDidFavoriteSucces(status: Bool)
    func presenterDidSaveFeed(status: Bool)
    func presenterDidRemoveFavorite(status: Bool)
}

class DetailsFeedPresenter {
    weak var view: DetailsFeedView?
    
    init(view: DetailsFeedView){
        self.view = view
    }
    
    func setFavorite(parameter : [String : Any]){
        HomeService.addFavorites(parameter: parameter) { [weak self] (status) in
            self?.view?.presenterDidFavoriteSucces(status: status)
        } errorBlock: { [weak self] (error) in
            let errorMessage = error ?? Localizables.Alert.standardError
            self?.view?.presenterDidFail(errorMessage: errorMessage)
        }
    }
    
    func saveLocalFavorites(feed: FeedModel) {
        var query = favoritesFeedData.getAll()
        if query.count == 0 {
            self.view?.presenterDidSaveFeed(status: favoritesFeedData.create(feed: feed))
        }else {
            query = favoritesFeedData.get(withPredicate: NSPredicate(format: "id == %@", "\(feed.id)"))
            if query.count == 0 {
                self.view?.presenterDidSaveFeed(status: favoritesFeedData.create(feed: feed))
            }else {
                self.view?.presenterDidSaveFeed(status: false)
            }
        }
    }
    
    func checkExistFeed(feed: FeedModel) -> Bool {
        var query = favoritesFeedData.getAll()
        query = favoritesFeedData.get(withPredicate: NSPredicate(format: "id == %@", "\(feed.id)"))
        return query.count > 0
    }
    
    func checkExistFeed(feed: LocalFavoriteFeed) -> Bool {
        var query = favoritesFeedData.getAll()
        query = favoritesFeedData.get(withPredicate: NSPredicate(format: "id == %@", "\(feed.id ?? "")"))
        return query.count > 0
    }
    
    func removeFavorite(feed: FeedModel) {
        var query = favoritesFeedData.getAll()
        query = favoritesFeedData.get(withPredicate: NSPredicate(format: "id == %@", "\(feed.id)"))
        guard let feedToDeleteFromFavorites = query.first else {
            self.view?.presenterDidRemoveFavorite(status: false)
            return
        }
        self.view?.presenterDidRemoveFavorite(status: favoritesFeedData.delete(id: feedToDeleteFromFavorites.objectID))
    }
    
    func removeFavorite(feed: LocalFavoriteFeed) {
        var query = favoritesFeedData.getAll()
        query = favoritesFeedData.get(withPredicate: NSPredicate(format: "id == %@", "\(feed.id ?? "")"))
        guard let feedToDeleteFromFavorites = query.first else {
            self.view?.presenterDidRemoveFavorite(status: false)
            return
        }
        self.view?.presenterDidRemoveFavorite(status: favoritesFeedData.delete(id: feedToDeleteFromFavorites.objectID))
    }
}

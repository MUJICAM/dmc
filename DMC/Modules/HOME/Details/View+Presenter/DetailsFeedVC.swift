//
//  DetailsFeedVC.swift
//  DMC
//
//  Created by MUJICAM on 9/1/22.
//

import UIKit
import SDWebImage

protocol detailsDelegate: AnyObject {
    func reloadTable()
}

class DetailsFeedVC: UIViewController {
    
    @IBOutlet weak var imageFeed: UIImageView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var buttonMenu: UIButton!
    
    @IBOutlet weak var labelFeedTitle: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDescriptionTitle: UILabel!
    @IBOutlet weak var labelFeedDate: UILabel!
    @IBOutlet weak var textViewFeedDescription: UITextView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var feed: FeedModel?
    var localFeed: LocalFavoriteFeed?
    var isLocalFeed: Bool = false
    var delegate: detailsDelegate?
    
    private var presenter: DetailsFeedPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    
    func setup() {
        setNav(
            icon: Images.icon(type: .backWhite),
            to: .leftButton,
            target: self,
            action: #selector(backButtonPressed(_:)),
            color: .white
        )
        
        presenter = DetailsFeedPresenter(view: self)
        setupUI()
    }
    
    func setupUI() {
        loader.stopAnimating()
        imageFeed.image = nil
        imageFeed.contentMode = .scaleAspectFit
        labelFeedDate.text = "loading..."
        labelFeedTitle.text = "loading..."
        labelName.text = "loading..."
        labelDescriptionTitle.text = Localizables.detailsFeed.description
        textViewFeedDescription.text = "loading..."
        textViewFeedDescription.backgroundColor = .clear
        buttonMenu.titleLabel?.text = ""
        buttonMenu.setTitle("", for: .normal)
        buttonMenu.addTarget(self, action: #selector(MenuButtonPressed(_:)), for: .touchUpInside)
        
        labelName.font = setFont(of: .bold, and: 14)
        labelName.textColor = .white
        
        labelFeedTitle.font = setFont(of: .bold, and: 16)
        labelFeedTitle.textColor = .white
        labelFeedTitle.numberOfLines = 0
        
        labelFeedDate.font = setFont(of: .semibold, family: .Mono,and: 10)
        labelFeedDate.textColor = .yellow.withAlphaComponent(0.35)
        
        labelDescriptionTitle.font = setFont(of: .bold, and: 16)
        labelDescriptionTitle.textColor = .white
        
        textViewFeedDescription.font = setFont(of: .bold, and: 14)
        textViewFeedDescription.textColor = .white
        textViewFeedDescription.isEditable = false
        if let feed = feed {
            isLocalFeed = false
            setValues(feed: feed)
        }else if let feed = localFeed {
            isLocalFeed = true
            setValues(feed: feed)
        }
    }
    
    func setValues(feed: FeedModel) {
        labelName.text = "\(feed.contact.firstName) \(feed.contact.lastName)"
        labelFeedTitle.text = feed.title
        labelFeedDate.text = feed.formattedDate
        
        imageFeed.sd_setImage(
            with: feed.imageURL,
            placeholderImage: Images.image(type: .login),
            options: [],
            completed: nil
        )
        textViewFeedDescription.attributedText = setHTMLDescription(with: feed.description)
    }
    
    func setValues(feed: LocalFavoriteFeed) {
        labelName.text = "\(feed.contact?.firstName ?? "") \(feed.contact?.lastName ?? "")"
        labelFeedTitle.text = feed.title
        
        imageFeed.sd_setImage(
            with: URL(string: feed.image ?? ""),
            placeholderImage: Images.image(type: .login),
            options: [],
            completed: nil
        )
        
        let serverFormat = DateFormatter()
        serverFormat.dateFormat = Constants.serverTimeFormat
        serverFormat.timeZone = TimeZone(identifier: "GMT")
        
        if let date = serverFormat.date(from: feed.published ?? "") {
            labelFeedDate.text = date.timeAgoDisplay()
        }
        textViewFeedDescription.attributedText = setHTMLDescription(with: feed.descriptionFeed ?? "")
    }

    //MARK: - @objc
    @objc func backButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func MenuButtonPressed(_ sender: UIButton) {
        let alert = UIAlertController(
            title: Localizables.modalsMessage.menu,
            message: Localizables.modalsMessage.addFavoritesMessage,
            preferredStyle: .alert
        )
        
        if isLocalFeed {
            if let feed = localFeed, let status = self.presenter?.checkExistFeed(feed: feed), status {
                alert.addAction(UIAlertAction(
                    title: Localizables.modalsMessage.removeFavorite,
                    style: .destructive,
                    handler: { [weak self] (UIAlertAction) in
                        self?.presenter?.removeFavorite(feed: self?.localFeed ?? LocalFavoriteFeed())
                    }))
            }
        }else {
            
            if let feed = feed, let status = self.presenter?.checkExistFeed(feed: feed), status {
                alert.addAction(UIAlertAction(
                    title: Localizables.modalsMessage.removeFavorite,
                    style: .destructive,
                    handler: { [weak self] (UIAlertAction) in
                        self?.presenter?.removeFavorite(feed: self?.feed ?? FeedModel())
                    }))
            }else {
                
                alert.addAction(UIAlertAction(
                    title: Localizables.modalsMessage.addFavorites,
                    style: .default ,
                    handler:{ [weak self] (UIAlertAction) in
                        self?.loader.startAnimating()
                        let parameter: [String : Any] = [
                            "postid" : self?.feed?.id ?? self?.localFeed?.id ?? ""
                        ]
                        self?.presenter?.setFavorite(parameter: parameter)
                    }))
            }
        }
        
        
        alert.overrideUserInterfaceStyle = .dark
        alert.addAction(UIAlertAction(
            title: Localizables.Alert.cancel,
            style: .cancel ,
            handler:{ (UIAlertAction) in }))
        
        self.present(alert, animated: true, completion: { })
    }
}

extension DetailsFeedVC: DetailsFeedView {
    
    func presenterDidRemoveFavorite(status: Bool) {
        Toast (
            text: Localizables.Alert.saveSuccess,
            container: navigationController,
            backgroundColor: Colors.lightBlue
        )
        
        if isLocalFeed {
            navigationController?.popViewController(animated: true, completion: {
                self.delegate?.reloadTable()
            })
        }
    }
    
    
    func presenterDidSaveFeed(status: Bool) {
        loader.stopAnimating()
        Toast (
            text: status ? Localizables.Alert.saveSuccess : Localizables.Alert.existsFavorites,
            container: navigationController,
            backgroundColor: status ? Colors.lightBlue : Colors.lightRed
        )
    }
    
    
    func presenterDidFavoriteSucces(status: Bool) {
        loader.stopAnimating()
        if status {
            presenter?.saveLocalFavorites(feed: feed ?? FeedModel())
        }else {
            Toast (
                text: Localizables.Alert.standardError,
                container: navigationController,
                backgroundColor: Colors.lightRed
            )
        }
    }
    
    func presenterDidFail(errorMessage: String) {
        loader.stopAnimating()
        Toast (
            text: errorMessage,
            container: navigationController,
            backgroundColor: Colors.lightRed
        )
    }
    
}

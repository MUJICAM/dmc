//
//  HomeService.swift
//  DMC
//
//  Created by MUJICAM on 8/1/22.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class HomeService {
    
    static func getFeed(successBlock: @escaping(_ feeds: [FeedModel]) -> Void, errorBlock: @escaping(_ error:  String?) -> Void ) {
        let path = API.Feed.getPath(for: .getFeed)
        Alamofire.request(
            path,
            method: .get,
            parameters: nil,
            encoding: JSONEncoding.default,
            headers: nil)
            .responseArray { (response: DataResponse<[FeedModel]>) in
                switch response.result {
                    
                case .success(let array):
                    successBlock(array)
                    break
                case .failure(let error):
                    errorBlock(error.localizedDescription)
                    break
                }
            }
    }
    
    static func getContacts(successBlock: @escaping(_ contacts: [Contacts]) -> Void, errorBlock: @escaping(_ error:  String?) -> Void ) {
        let path = API.Contact.getPath(for: .getContact)
        Alamofire.request(
            path,
            method: .get,
            parameters: nil,
            encoding: JSONEncoding.default,
            headers: nil)
            .responseArray { (response: DataResponse<[Contacts]>) in
                switch response.result {
                    
                case .success(let array):
                    successBlock(array)
                    Storage.contact = array
                    break
                case .failure(let error):
                    errorBlock(error.localizedDescription)
                    break
                }
            }
    }
    
    static func addFavorites(parameter: [String : Any], successBlock: @escaping(_ status: Bool) -> Void, errorBlock: @escaping(_ error:  String?) -> Void ) {
        let path = API.Favorites.getPath(for: .setFavorites)
        let headers = API.tokenHeaders(withToken: Storage.sessionData?.apiToken ?? "")
        
        Alamofire.request(
            path,
            method: .post,
            parameters: parameter,
            encoding: JSONEncoding.default,
            headers: headers)
            .responseJSON (completionHandler: {response in
                switch response.result {
                case .success(let value):
                    if let result = value as? [String: Any],
                        let statusString = result[API.status.uppercased()] as? String{
                        if statusString == "OK" {
                            successBlock(true)
                        }else{
                            if let result = value as? [String: Any],
                               let message = result[API.message] as? String {
                                errorBlock(message)
                            }
                        }
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            })
    }
    
}

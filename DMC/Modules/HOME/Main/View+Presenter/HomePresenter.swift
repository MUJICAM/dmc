//
//  HomePresenter.swift
//  DMC
//
//  Created by MUJICAM on 8/1/22.
//

import Foundation
import UIKit

protocol HomeView: AnyObject {
    func presenterDidSuccess(fedd: [FeedModel])
    func presenterDidFail(errorMessage: String)
    func presenterDidFavoriteSucces(status: Bool)
    func presenterDidSaveFeed(status: Bool)
    func presenterDidRemoveFavorite(status: Bool)
}

class HomePresenter {
    weak var view: HomeView?
    
    init(view: HomeView?) {
        self.view = view
    }
    
    func getFeed(){
        HomeService.getFeed { [weak self] feeds in
            self?.getContact(feeds: feeds)
        } errorBlock: { [weak self] (error) in
            let errorMessage = error ?? Localizables.Alert.standardError
            self?.view?.presenterDidFail(errorMessage: errorMessage)
        }
    }
    
    func getContact(feeds: [FeedModel]) {
        LaunchService.getContacts { [weak self] (contacts) in
            Storage.contact = contacts
            for contact in contacts {
                for feed in feeds {
                    if contact.id == Int(feed.author_id) {
                        feed.contact = contact
                    }
                }
            }
            self?.view?.presenterDidSuccess(fedd: feeds)
        } errorBlock: { [weak self] (error) in
            let errorMessage = error ?? Localizables.Alert.standardError
            self?.view?.presenterDidFail(errorMessage: errorMessage)
        }
    }
    
    func setFavorite(parameter : [String : Any]){
        HomeService.addFavorites(parameter: parameter) { [weak self] (status) in
            self?.view?.presenterDidFavoriteSucces(status: status)
        } errorBlock: { [weak self] (error) in
            let errorMessage = error ?? Localizables.Alert.standardError
            self?.view?.presenterDidFail(errorMessage: errorMessage)
        }
    }
    
    func saveLocalFavorites(feed: FeedModel) {
        var query = favoritesFeedData.getAll()
        if query.count == 0 {
            self.view?.presenterDidSaveFeed(status: favoritesFeedData.create(feed: feed))
        }else {
            query = favoritesFeedData.get(withPredicate: NSPredicate(format: "id == %@", "\(feed.id)"))
            if query.count == 0 {
                self.view?.presenterDidSaveFeed(status: favoritesFeedData.create(feed: feed))
            }else {
                self.view?.presenterDidSaveFeed(status: false)
            }
        }
    }
    
    func checkExistFeed(feed: FeedModel) -> Bool {
        var query = favoritesFeedData.getAll()
        query = favoritesFeedData.get(withPredicate: NSPredicate(format: "id == %@", "\(feed.id)"))
        return query.count > 0
    }
    
    func removeFavorite(feed: FeedModel) {
        var query = favoritesFeedData.getAll()
        query = favoritesFeedData.get(withPredicate: NSPredicate(format: "id == %@", "\(feed.id)"))
        guard let feedToDeleteFromFavorites = query.first else {
            self.view?.presenterDidRemoveFavorite(status: false)
            return
        }
        self.view?.presenterDidRemoveFavorite(status: favoritesFeedData.delete(id: feedToDeleteFromFavorites.objectID))        
    }
}

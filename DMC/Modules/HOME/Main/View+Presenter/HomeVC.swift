//
//  HomeVC.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import UIKit

class HomeVC: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var cells: [CellsEnum] = []
    var presenter: HomePresenter?
    var feedList: [FeedModel] = []
    var feedSelected: FeedModel = FeedModel()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
        tableView.fadeIn(at: 0.2)
    }
    
    private func setupUI(){
        navigationController?.setNavigationBarHidden(false, animated: false)
        setNav(
            icon: Images.icon(type: .logout),
            to: .rightButton,
            target: self,
            action: #selector(logutButtonPressed(_:)),
            color: Colors.lightRed
        )
        loader.stopAnimating()
        presenter = HomePresenter(view: self)
        getFeed()
    }
    
    private func getFeed() {
        loader.startAnimating()
        presenter?.getFeed()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        for cell in cells {
            tableView.registerCell(named: Cells.identifiersMethod(type: cell))
        }
    }
    
    //MARK: - @objc
    @objc func logutButtonPressed(_ sender: UIButton) {
        let alert = UIAlertController(
            title: Localizables.modalsMessage.logout,
            message: Localizables.modalsMessage.logout_Message,
            preferredStyle: .actionSheet
        )
        
        alert.addAction(UIAlertAction(
            title: Localizables.modalsMessage.yes,
            style: .destructive ,
            handler:{ (UIAlertAction) in
                AppManager.shared.userDidLogout()
            }))
        
        alert.overrideUserInterfaceStyle = .light
        alert.addAction(UIAlertAction(
            title: Localizables.Alert.cancel,
            style: .cancel ,
            handler:{ (UIAlertAction) in }))
        
        self.present(alert, animated: true, completion: { })
    }
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let type = cells[index]
        let id = Cells.identifiersMethod(type: type)
        switch type {
        case .feed(let feed):
            let cell = tableView.dequeueReusableCell(withIdentifier: id) as! FeedCell
            cell.setupCell(feed: feed)
            cell.selectionStyle = .none
            cell.delegate = self
            return cell
        default:
            let cell = UITableViewCell()
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ViewFactory.viewControllerForAppView(.details) as! DetailsFeedVC
        vc.feed = feedList[indexPath.row]
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}



extension HomeVC: HomeView {
    
    func presenterDidRemoveFavorite(status: Bool) {
        Toast (
            text: Localizables.Alert.saveSuccess,
            container: navigationController,
            backgroundColor: Colors.lightBlue
        )
    }
    
    
    func presenterDidSaveFeed(status: Bool) {
        loader.stopAnimating()
        Toast (
            text: status ? Localizables.Alert.saveSuccess : Localizables.Alert.existsFavorites,
            container: navigationController,
            backgroundColor: status ? Colors.lightBlue : Colors.lightRed
        )
    }
    
    
    func presenterDidFavoriteSucces(status: Bool) {
        loader.stopAnimating()
        if status {
            presenter?.saveLocalFavorites(feed: feedSelected)
        }else {
            Toast (
                text: Localizables.Alert.standardError,
                container: navigationController,
                backgroundColor: Colors.lightRed
            )
        }
    }
    
    func presenterDidSuccess(fedd: [FeedModel]) {
        loader.stopAnimating()
        feedList = fedd
        
        for feed in feedList {
            cells.append(.feed(feed))
        }
        
        setupTableView()
        tableView.reloadData()
    }
    
    func presenterDidFail(errorMessage: String) {
        loader.stopAnimating()
        Toast (
            text: errorMessage,
            container: navigationController,
            backgroundColor: Colors.lightRed
        )
    }
    
}

extension HomeVC: FeedDelegate {
    
    func favoritesButtonPressed(_ sender: UIButton, cell: FeedCell, feed: FeedModel) {
        let alert = UIAlertController(
            title: Localizables.modalsMessage.menu,
            message: Localizables.modalsMessage.addFavoritesMessage,
            preferredStyle: .alert
        )
        
        
        if let status = self.presenter?.checkExistFeed(feed: feed), status {
            alert.addAction(UIAlertAction(
                title: Localizables.modalsMessage.removeFavorite,
                style: .destructive,
                handler: { [weak self] (UIAlertAction) in
                    self?.presenter?.removeFavorite(feed: feed)
                }))
        }else {
            alert.addAction(UIAlertAction(
                title: Localizables.modalsMessage.addFavorites,
                style: .default ,
                handler:{ [weak self] (UIAlertAction) in
                    self?.feedSelected = feed
                    self?.loader.startAnimating()
                    let parameter: [String : Any] = [
                        "postid" : feed.id
                    ]
                    self?.presenter?.setFavorite(parameter: parameter)
                }))
        }
        
        alert.overrideUserInterfaceStyle = .dark
        alert.addAction(UIAlertAction(
            title: Localizables.Alert.cancel,
            style: .cancel ,
            handler:{ (UIAlertAction) in }))
        
        self.present(alert, animated: true, completion: { })
    }
}

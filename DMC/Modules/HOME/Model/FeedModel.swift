//
//  FeedModel.swift
//  DMC
//
//  Created by MUJICAM on 8/1/22.
//

import Foundation
import ObjectMapper


class FeedModel: Mappable {
    
    struct JSONKeys {
        static let id = "id"
        static let title = "title"
        static let image = "image"
        static let date = "date"
        static let description = "description"
        static let published = "published"
        static let author_id = "author_id"
        static let contact = "contact"
    }
    
    var id : Int = 0
    var title: String = ""
    var image: String = ""
    var date: String = ""
    var description: String = ""
    var published: String = ""
    var author_id: String = ""
    var contact: Contacts = Contacts()
    
    var imageURL: URL? {
        return URL(string: image)
    }
    
    var formattedDate: String {
        let serverFormat = DateFormatter()
        serverFormat.dateFormat = Constants.serverTimeFormat
        serverFormat.timeZone = TimeZone(identifier: "GMT")
        
        if let date = serverFormat.date(from: self.published) {
            return date.timeAgoDisplay()
        }
        return ""
    }
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        id <- map[JSONKeys.id]
        title <- map[JSONKeys.title]
        image <- map[JSONKeys.image]
        date <- map[JSONKeys.date]
        description <- map[JSONKeys.description]
        published <- map[JSONKeys.published]
        author_id <- map[JSONKeys.author_id]
    }
    
}

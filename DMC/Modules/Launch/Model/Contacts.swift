//
//  Contacts.swift
//  DMC
//
//  Created by MUJICAM on 9/1/22.
//

import Foundation
import ObjectMapper


class Contacts: Mappable {
    
    struct JSONKeys {
        static let id = "id"
        static let firstName = "firstName"
        static let lastName = "lastName"
        static let gender = "gender"
    }
    
    var id: Int = 0
    var firstName: String = ""
    var lastName: String = ""
    var gender: String = ""
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        id <- map[JSONKeys.id]
        firstName <- map[JSONKeys.firstName]
        lastName <- map[JSONKeys.lastName]
        gender <- map[JSONKeys.gender]
    }
}

//
//  LaunchService.swift
//  DMC
//
//  Created by MUJICAM on 9/1/22.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class LaunchService {
    
    static func getContacts(successBlock: @escaping(_ contacts: [Contacts]) -> Void, errorBlock: @escaping(_ error:  String?) -> Void ) {
        let path = API.Contact.getPath(for: .getContact)
        Alamofire.request(
            path,
            method: .get,
            parameters: nil,
            encoding: JSONEncoding.default,
            headers: nil)
            .responseArray { (response: DataResponse<[Contacts]>) in
                switch response.result {
                    
                case .success(let array):
                    successBlock(array)
                    Storage.contact = array
                    break
                case .failure(let error):
                    errorBlock(error.localizedDescription)
                    break
                }
            }
    }
}

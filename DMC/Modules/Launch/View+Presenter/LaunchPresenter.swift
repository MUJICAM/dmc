//
//  LaunchPresenter.swift
//  DMC
//
//  Created by MUJICAM on 9/1/22.
//

import Foundation

protocol LaunchView: AnyObject {
    func presenterDidSuccess(fedd: [Contacts])
    func presenterDidFail(errorMessage: String)
}

class LaunchPresenter {
    weak var launchView: LaunchView?
    
    init(launchView: LaunchView) {
        self.launchView = launchView
    }
    
    func getContact() {
        LaunchService.getContacts { [weak self] (contacts) in
            Storage.contact = contacts
            self?.launchView?.presenterDidSuccess(fedd: contacts)
        } errorBlock: { [weak self] (error) in
            let errorMessage = error ?? Localizables.Alert.standardError
            self?.launchView?.presenterDidFail(errorMessage: errorMessage)
        }
    }
}

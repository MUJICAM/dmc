//
//  LaunchVC.swift
//  DMC
//
//  Created by MUJICAM on 8/1/22.
//

import UIKit
import Lottie

class LaunchVC: UIViewController {
    
    private var animationView: AnimationView?
    
    var tasksAmount: Int = 3
    private var _tasksCounter: Int = 0
    var tasksCounter: Int {
        get {
            return _tasksCounter
        }
        
        set {
            _tasksCounter = newValue
            if _tasksCounter >= tasksAmount {
                AppManager.shared.resetRootController()
            }
        }
    }
    var presenter: LaunchPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lotieAnimation()
        presenter = LaunchPresenter(launchView: self)
        presenter?.getContact()
    }
    
    
    func lotieAnimation() {
        
        animationView = .init(name: "test")
        let animationWidth: CGFloat = UIScreen.main.bounds.width
        let animationHeight: CGFloat = animationWidth
        let x = UIScreen.main.bounds.width/2 - animationWidth/2
        let y = UIScreen.main.bounds.height/2 - animationHeight/2
        animationView?.frame = CGRect(x: x, y: y, width: animationWidth, height: animationHeight)
        
        animationView?.contentMode = .scaleAspectFill
        animationView?.loopMode = .loop
        animationView?.animationSpeed = 3
        
        view.addSubview(animationView!)
        
        animationView?.play(fromProgress: animationView?.currentProgress,
                            toProgress: 1, loopMode: .playOnce) { [weak self] completed in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
                self?.animationView?.stop()
//                AppManager.shared.resetRootController()
            }
        }
    }
}

extension LaunchVC: LaunchView {
    
    func presenterDidSuccess(fedd: [Contacts]) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {            
            self.animationView?.stop()
            AppManager.shared.resetRootController()
        }
    }
    
    func presenterDidFail(errorMessage: String) {
        print(errorMessage)
    }
}

//
//  SignUser.swift
//  DMC
//
//  Created by MUJICAM on 8/1/22.
//

import Foundation
import ObjectMapper

class SignUser: Mappable {
    
    struct JSONKeys {
        static let apiToken = "api-token"
        static let status = "STATUS"
        static let error = "error"
        static let message = "message"
    }
    
    var apiToken: String?
    var status: String?
    var error: String?
    var message: String?
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        apiToken <- map[JSONKeys.apiToken]
        status <- map[JSONKeys.status]
        error <- map[JSONKeys.error]
        message <- map[JSONKeys.message]
    }
}

//
//  AuthServices.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class AuthServices {
    static func logIn(parameters: Parameters,  successBlock: @escaping(_ user: SignUser) -> Void, errorBlock: @escaping(_ error:  String?) -> Void ) {
    
        let path = API.Auth.getPath(for: .signIN)
        Alamofire.request(
            path,
            method: .post,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: nil
        )
        .responseJSON(completionHandler: { response in
            switch response.result {
            case .success:
                if let responseDict = response.value as? [String: Any] {
                    if let partialUser = SignUser(JSON: responseDict), let status = partialUser.status, status != API.error.uppercased() {
                        successBlock(partialUser)
                    } else if let error = responseDict[API.message] as? String {
                        errorBlock(error)
                    }
                } else {
                    errorBlock(nil)
                }
            case .failure:
                errorBlock(nil)
            }
        })
    }
}

//
//  LoginPresenter.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import Foundation
import UIKit

protocol LoginView: AnyObject {
    func presenterDidLogin()
    func presenterDidFail(errorMessage: String)
    func didPresenterFindEmptyFields(status: Bool)
}

class LoginPresenter {
    weak var view: LoginView?
    let testCases: [CellsStyles] = [.password]
    var userInfo: [String: String] = [:]
    
    init(view: LoginView?) {
        self.view = view
    }
    
    init() {}
    
    func checkForEmptyFields(with dict: [CellsStyles: String]) {
        let password = dict[.password, default: ""]
        let username = dict[.username, default: ""]
        
        if password.isEmpty || username.isEmpty {
            self.view?.didPresenterFindEmptyFields(status: true)
            return
        }
        
        self.view?.didPresenterFindEmptyFields(status: false)
    }
    
    func validateData(with dict: [CellsStyles: String]) {
        let strings = Localizables.ValidationMessages.self
        var localDict = dict
        localDict[.username] = dict[.username, default: ""]
        localDict[.password] = dict[.password, default: ""]
        
        for style in testCases {
            var value = localDict[style] ?? ""
            if style == .username{
                value = dict[.username, default: ""]
            }else {
                value = dict[.password, default: ""]
            }
            if value.isEmpty {
                let formattedMessage = String(format: strings.emptyValue, "\(style)".camelCaseToWords().lowercased())
                self.view?.presenterDidFail(errorMessage: formattedMessage)
                return
            }
        }
        
        var stringDict: [String: String] = [:]
        for (style, value) in localDict {
            stringDict[Cells.styleToKey(style: style)] = value
        }
        userInfo = stringDict
        login(userInfo: stringDict)
    }
    
    private func login(userInfo: [String: String]){
        self.userInfo = userInfo
        AuthServices.logIn(parameters: userInfo, successBlock: { [weak self] (sessionData) in
            Storage.sessionData = sessionData
            self?.view?.presenterDidLogin()
        }) { [weak self] (error) in
            let errorMessage = error ?? Localizables.Alert.standardError
            self?.view?.presenterDidFail(errorMessage: errorMessage)
        }
    }
}

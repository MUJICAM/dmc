//
//  LoginVC.swift
//  DMC
//
//  Created by MUJICAM on 7/1/22.
//

import UIKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var presenter: LoginPresenter?
    var buttonCell: ButtonCell?
    var userInfo: [CellsStyles: String] = [:]
    let cells: [CellsEnum] = [
        .blankSpace(.small),
        .image(.login),
        .blankSpace(.small),
        .textField(.username),
        .textField(.password),
        .blankSpace(.small),
        .button(.login)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = LoginPresenter(view: self)
        loader.stopAnimating()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
        tableView.fadeIn(at: 0.2)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: - Private methods
    private func setupUI(){
        navigationController?.setNavigationBarHidden(true, animated: false)
        tableView.alpha = 0
        tableView.delegate = self
        tableView.dataSource = self
        for cell in cells {
            tableView.registerCell(named: Cells.identifiersMethod(type: cell))
        }
    }
    
}

extension LoginVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let type = cells[index]
        let id = Cells.identifiersMethod(type: type)
        switch type {
        case .textField(let style):
            let cell = tableView.dequeueReusableCell(withIdentifier: id) as! TextFieldCell
            cell.setupCell(with: style, showValidationMessage: false)
            cell.selectionStyle = .none
            cell.textField.text = userInfo[style, default: ""]
            cell.delegate = self
            return cell
            
        case .button(let style):
            let cell = tableView.dequeueReusableCell(withIdentifier: id) as! ButtonCell
            cell.selectionStyle = .none
            cell.setupCell(with: style)
            cell.delegate = self
            if style == .login {
                buttonCell = cell
            }
            
            presenter?.checkForEmptyFields(with: userInfo)
            return cell
            
        case .image(let style):
            let cell = tableView.dequeueReusableCell(withIdentifier: id) as! ImageCell
            cell.selectionStyle = .none
            cell.setupCell(with: style)
            return cell
        default:
            let cell = UITableViewCell()
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let preferredHeight = Cells.preferredHeight(type: cells[indexPath.row])
        if preferredHeight == 0.0 { return UITableView.automaticDimension }
        return preferredHeight
    }
    
}

extension LoginVC: TextFieldCellDelegate {
    func textFieldDidChange(_ textField: UITextField, cell: TextFieldCell, style: CellsStyles) {
        let text = textField.text ?? ""
        userInfo[style] = text
        presenter?.checkForEmptyFields(with: userInfo)
    }
}

extension LoginVC: ButtonCellDelegate {
    
    func mainButtonPressed(_ sender: UIButton, cell: ButtonCell, style: CellsStyles) {
        switch style {
        case .login:
            buttonCell?.isEnabled = false
            loader.startAnimating()
            presenter?.validateData(with: userInfo)
            break
        default:
            break
        }
    }
}

extension LoginVC: LoginView {
    
    func presenterDidLogin() {
        AppManager.shared.resetRootController()
    }
    
    func presenterDidFail(errorMessage: String) {
        loader.stopAnimating()
        buttonCell?.isEnabled = true
        Toast (
            text: errorMessage,
            container: navigationController,
            backgroundColor: Colors.lightRed
        )
    }
    
    func didPresenterFindEmptyFields(status: Bool) {
        buttonCell?.isEnabled = !status
    }

}

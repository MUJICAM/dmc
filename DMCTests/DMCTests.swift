//
//  DMCTests.swift
//  DMCTests
//
//  Created by MUJICAM on 7/1/22.
//

import XCTest
@testable import DMC

class DMCTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
    }
    

    func testLoginCheckForEmptyFields() {
        let presenter = LoginPresenter(view: self)
        let infoLogin : [CellsStyles : String] = [
            .username : "algo",
            .password : "algo"
        ]
        presenter.checkForEmptyFields(with: infoLogin)
    }
    
    func testLoginDidFail() {
        let presenter = LoginPresenter(view: self)
        let infoLogin : [CellsStyles : String] = [
            .username : "algo",
            .password : "algo"
        ]
        presenter.validateData(with: infoLogin)
    }
    
    func testLoginDidLogin() {
        let presenter = LoginPresenter(view: self)
        let infoLogin : [CellsStyles : String] = [
            .username : "juan@empresa.cl",
            .password : "mi_password"
        ]
        presenter.validateData(with: infoLogin)
    }
}

extension DMCTests: LoginView {
    
    func presenterDidLogin() {
        XCTAssertTrue(true)
    }
    
    func presenterDidFail(errorMessage: String) {
        XCTAssertNil(errorMessage)
    }
    
    func didPresenterFindEmptyFields(status: Bool) {
        XCTAssertFalse(status)
    }
    
    
}

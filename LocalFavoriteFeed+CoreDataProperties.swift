//
//  LocalFavoriteFeed+CoreDataProperties.swift
//  
//
//  Created by MUJICAM on 9/1/22.
//
//

import Foundation
import CoreData


extension LocalFavoriteFeed {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LocalFavoriteFeed> {
        return NSFetchRequest<LocalFavoriteFeed>(entityName: "LocalFavoriteFeed")
    }

    @NSManaged public var author_id: String?
    @NSManaged public var published: String?
    @NSManaged public var descriptionFeed: String?
    @NSManaged public var date: String?
    @NSManaged public var image: String?
    @NSManaged public var title: String?
    @NSManaged public var id: String?

}
